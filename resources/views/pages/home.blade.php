@once
  @push('head scripts')
    <script src="{{ asset('js/plugins/swiper-bundle.min.js') }}"></script>
  @endpush
@endonce

@once
  @push('styles')
    <link rel="stylesheet" href="{{ asset('css/plugins/swiper-bundle.min.css') }}">
  @endpush
@endonce

<x-app-layout>

  <x-home-banner :photos="$photos"></x-home-banner>


  <section class="home-list-conllection py-12">
    <x-container>
      <div class="grid gap-6 grid-cols-6 grid-rows-2">
        {{-- $collections limit 3 value --}}
        @foreach ($collections as $collection)
          @if ($loop->first)
            <div class="col-span-6 md:col-span-3 md:row-span-2 lg:col-span-2 group">
              <a href="{{ route('collections', $collection->slug) }}" class="relative block pb-[125%] overflow-hidden">
          @elseif ($loop->last)
            <div class="col-span-3 md:row-span-1 lg:col-span-2 lg:row-span-2 group">
              <a href="{{ route('collections', $collection->slug) }}" class="relative block pb-[125%] md:pb-[calc(62.5%-12px)] lg:pb-[125%] overflow-hidden">
          @else
            <div class="col-span-3 md:row-span-1 lg:col-span-2 lg:row-span-2 group">
              <a href="{{ route('collections', $collection->slug) }}" class="relative block pb-[125%] md:pb-[calc(62.5%-12px)] lg:pb-[125%] overflow-hidden">
          @endif

                <div class="absolute w-full h-full top-0 left-0 group-hover:scale-150 transition-transform duration-75 group-hover:duration-[5s] ease-linear">
                  <img src="{{ Voyager::image($collection->getImage())}}" alt="" class="w-full h-full object-cover" loading="lazy">
                </div>

                <div class="absolute w-full h-full top-0 left-0 transition-colors duration-300 ease-in-out group-hover:bg-black/70 pointer-events-none flex items-end px-4 py-10">
                  <h2 class="text-3xl font-bold uppercase text-white">{{ $collection->title }}</h2>
                </div>
              </a>
            </div>
        @endforeach
      </div>
    </x-container>
  </section>


  <section class="doi-tac">
    <x-container>
      <div class="">Đối tác của chúng tôi</div>
    </x-container>
  </section>
</x-app-layout>
