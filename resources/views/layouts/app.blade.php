<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('css/plugins/boxicons-2.0.9/css/boxicons.min.css') }}">

  @stack('styles')

  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <link rel="stylesheet" href="{{ asset('css/main.css') }}">
  <style>
    :root {
      @if (setting('site.color') == 'violet')
        --color-primary-50: #F5F3FF;
        --color-primary-100: #EDE9FE;
        --color-primary-200: #DDD6FE;
        --color-primary-300: #C4B5FD;
        --color-primary-400: #A78BFA;
        --color-primary-500: #8B5CF6;
        --color-primary-600: #7C3AED;
        --color-primary-700: #6D28D9;
        --color-primary-800: #5B21B6;
        --color-primary-900: #4C1D95;
      @elseif (setting('site.color') == 'amber')
        --color-primary-50: #FFFBEB;
        --color-primary-100: #FEF3C7;
        --color-primary-200: #FDE68A;
        --color-primary-300: #FCD34D;
        --color-primary-400: #FBBF24;
        --color-primary-500: #F59E0B;
        --color-primary-600: #D97706;
        --color-primary-700: #B45309;
        --color-primary-800: #92400E;
        --color-primary-900: #78350F;
      @elseif (setting('site.color') == 'green')
        --color-primary-50: #F0FDF4;
        --color-primary-100: #DCFCE7;
        --color-primary-200: #BBF7D0;
        --color-primary-300: #86EFAC;
        --color-primary-400: #4ADE80;
        --color-primary-500: #22C55E;
        --color-primary-600: #16A34A;
        --color-primary-700: #15803D;
        --color-primary-800: #166534;
        --color-primary-900: #14532D;
      @else
        --color-primary-50: #EFF6FF;
        --color-primary-100: #DBEAFE;
        --color-primary-200: #BFDBFE;
        --color-primary-300: #93C5FD;
        --color-primary-400: #60A5FA;
        --color-primary-500: #3B82F6;
        --color-primary-600: #2563EB;
        --color-primary-700: #1D4ED8;
        --color-primary-800: #1E40AF;
        --color-primary-900: #1E3A8A;
      @endif
    }
  </style>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  @stack('head scripts')
</head>

<body class="font-sans antialiased w-full">
  <div class="min-h-screen flex flex-col bg-gray-100">
    @include('layouts.navigation')

    <!-- Page Heading -->
    @include('layouts.header')

    <!-- Page Content -->
    <main class="flex-grow min-w-0">
      {{ $slot }}
    </main>

     <!-- Page Footer -->
     @include('layouts.footer')
  </div>

  @stack('scripts')
</body>

</html>
