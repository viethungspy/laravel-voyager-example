<footer class="flex-none bg-white">
  <div class="w-full max-w-7xl mx-auto px-4 text-center py-3">
    Copyright &copy; {{ date('Y') }} Việt Hùng. All Rights Reserved
  </div>
</footer>
