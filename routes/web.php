<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CollectionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::redirect('/products', '/collections');
Route::get('/collections', [CollectionController::class, 'listCollections'])->name('list-collection');
Route::get('/collections/{collection-slug}', [CollectionController::class, 'index'])->name('collections');

Route::get('/products/{product-slug}', [CollectionController::class, 'index'])->name('products');
Route::get('/collections/{collection-slug}/products/{product-slug}', [CollectionController::class, 'index'])->name('collections-products');

Route::get('/dashboard', function () {
  return view('pages.dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';


Route::group(['prefix' => 'admin'], function () {
  Voyager::routes();
});

Route::get('/clear-cache', function () {
  Artisan::call('cache:clear');
  return "Cache is cleared";
});
